Game of Life written in Ruby.
=============================

If you are interested in the game, visit:
<a href="http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life">Game of Life</a>.

If you would like to know something more about the awesome guy that created it,
please visit <a href="http://en.wikipedia.org/wiki/John_Horton_Conway">John Conway's</a> wiki page.

-

This implementation has three parameters:<br>
1) number of life cycles that will occur throughout the game,<br>
2) pattern of an initial seed,<br>
3) dimension for a grid on an which the evolution of life will take place.<br>

If you want to explore how certain patterns evolve, simply insert
your desired pattern in seeds.rb file. Give it a name and call a
method in main.rb with that particular constant.

Tips:<br> 
1) set your terminal size to fit the chosen grid area so you can
   see only the last grid displayed by a game.<br>
2) you can also adjust the "speed" of a game by setting the time
   of the 'start' method execution in main.rb.<br>

written on 10.1.2014.
