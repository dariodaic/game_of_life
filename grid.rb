class Grid
  class <<self
    def construct_board side
      side.times.inject([]) do |grid|
        grid << side.times.inject([]) { |row| row << nil }
      end
    end
  end

  attr_reader :side, :board

  def initialize side
    @side = side
    @board = Grid.construct_board @side
  end

  def populate_with seeds
    seeds.each { |cell| @board[cell[0]][cell[1]] = "#" }
  end

  def cell_alive? cell
    !@board[cell[0]][cell[1]].nil?
  end

  def to_s
    grid_string = ""
    @board.each do |row|
      grid_string += row.collect { |cell| cell.nil? ? " " : "#" }
      .join + "\n"
    end
    puts grid_string
  end
end