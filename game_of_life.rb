class GameOfLife

  def initialize number_of_life_cycles
    @life_cycles = number_of_life_cycles
  end

  def start seeds, grid, n=0
    sleep 0.5 # adjust the speed of Ticks
    return puts "(Flatland implodes)" if n == @life_cycles
    puts "#{n}th generation:"
    grid.populate_with seeds
    grid.to_s

    next_seeds = []
    grid.side.times do |x|
      grid.side.times do |y|
        neighbours_count = collect_and_count_neighbours(grid, [x, y])

        if grid.cell_alive? [x, y]
          next_seeds << [x, y] if neighbours_count == 2 || neighbours_count == 3
        else
          next_seeds << [x, y] if neighbours_count == 3
        end
      end
    end

    start next_seeds, Grid.new(grid.side), n+=1
  end

  def collect_and_count_neighbours grid, cell
    x, y = cell
    neighbours = []

    (-1..1).each do |i|
      next if x + i < 0 || x + i > (grid.side - 1)
      (-1..1).each do |j|
        next if (y + j < 0 || y + j > (grid.side - 1)) || (i.zero? && j.zero?)
        neighbours << grid.board[x + i][y + j]
      end
    end
    neighbours.count { |cell| cell == "#" }
  end
end
