require './game_of_life'
require './grid'
require './seeds'

include InitialSeeds

GameOfLife.new(50).start PULSAR, Grid.new(20)